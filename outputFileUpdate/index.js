const sql = require('mssql');

exports.outputFileUpdate = async (context, req) => {
    try {
        let db = await sql.connect({
            user: 'cdgennpadmin',
            password: "qJ]%='L.<6t6;YY2",
            database: 'ava-eus-cdgen-np-sqldb',
            server: 'ava-eus-cdgen-np-sqlsrv.database.windows.net',
            // or
            // server: 'SEZLP366\\SQLEXPRESS',
            options: {
                encrypt: true, // for azure
                trustServerCertificate: true // change to true for local dev / self-signed certs
            }
        });
        let result;
        let reqData = req.body;
        // let CodeGenID = req.body.RequiredID;
        console.log(reqData);

        result = await db.query(`
            update  CodeGen set CodeStatus=${reqData.CodeStatus},ModifiedAt = getDate(), GeneratedCodePath= '${reqData.OutputFilename}' where CodeGenID = ${reqData.RequiredID}
         `)

        sql.close();
        console.log(result);
        let responseData = result.rowsAffected;
        //console.log(responseData);
        context.res = {
            body: {
                Success: true,
                ResponseData: responseData,
                ErrorMessage: null
            },
            headers: {
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET',
                'Content-Type': 'application/json',
                'Access-Control-Request-Headers': '*'

            }
            //console.log(responseData);
        }
    }
    catch (err) {
        context.res = {
            body: {
                Success: false,
                ResponseData: err.message,
                ErrorMessage: null
            },
            headers: {
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET',
                'Content-Type': 'application/json',
                'Access-Control-Request-Headers': '*'
            }
        }
    }
}